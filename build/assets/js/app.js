'use strict';

const activeClass  =   'is-active';
const fixedClass   =   'is-fixed';
const focusClass   =   'is-focused';
const hoverClass   =   'is-hover';
const visibleClass =   'is-visible';
const expandedClass =  'is-expanded';
const selectedClass =  'is-selected';
const collapsedClass = 'is-collapsed';
const scrolledClass =  'is-scrolled';
const headerFixedClass = 'header-is-fixed';
const lockedScrollClass = 'scroll-is-locked';
const searchVisibleClass = 'search-is-visible';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';
const dropdownMenuVisibleClass = 'dropdown-menu-is-visible';
const requestPopupVisibleClass = 'request-popup-is-visible'
const headerTransparentClass = 'header--transparent';

const $header = $('[data-component="header"]');
const $body = $('.body');
const $main = $('.main');

const bpSM = 520;
const bpMD = 768;
const bpLG = 1024;
const bpXL = 1220;
const bp2Xl = 1440;
const bp3XL = 1920;
// Amount form
$(document).ready(function() {
    $(document).on('click', function(e) {
        var target = $(e.target);
        var amountForm = "";

        if (e.target.dataset.component == "amount-form") {
            amountForm = target;
        } else if (target.parents('[data-component="amount-form"]').length) {
            amountForm = target.parents('[data-component="amount-form"]');
        }

        if (!amountForm) return;

        var
        amountInput = amountForm.find('[data-element="amount-input"]'),
        amountValue = amountInput.val(),
        amountValueNumber = +amountValue,
        amountMin = +amountInput.attr('data-min')  || 0,
        amountMax = +amountInput.attr('data-max'),
        amountStep = +amountInput.attr('data-step') || 1,
        amountNewValue = 0;

        amountInput.focus().val('').val(amountValue);

        if (target.attr('data-element') === 'inc-control' || target.attr('data-element') === 'dec-control') {
            if (target.attr('data-element') === 'inc-control') {
                amountForm.find('[data-element="dec-control"]').prop('disabled', false);
                if (amountMax && amountValueNumber >= amountMax) {
                    amountNewValue = amountValueNumber;
                    target.prop('disabled', true);
                } else {
                    amountNewValue = amountValueNumber + amountStep;
                }
                if (amountNewValue >= amountMax) {
                    target.prop('disabled', true);
                }
            } else if (target.attr('data-element') === 'dec-control') {
                amountForm.find('[data-element="inc-control"]').prop('disabled', false);
                if (amountValueNumber <= amountMin) {
                    amountNewValue = amountValueNumber;
                } else {
                    amountNewValue = amountValueNumber - amountStep;
                }
                if (amountNewValue <= amountMin) {
                    target.prop('disabled', true);
                }
            }
            if (Number.isInteger(amountStep)) {
                amountInput.val(amountNewValue);
            } else {
                amountInput.val(amountNewValue.toFixed(1));
            }
            amountInput.blur();
        } else {
            amountInput.focus().val('').val(amountValue);
        }
    });
});
/* Catalog slider */
(function() {
    let $catalogSlider = new Swiper('[data-slider="catalog"]', {
        navigation: {
            nextEl: '.catalog-slider-next',
            prevEl: '.catalog-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1220: {
                slidesPerView: 3,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();
/* Clients slider */
(function() {
    let $clientsSlider = new Swiper('[data-slider="clients"]', {
        navigation: {
            nextEl: '.clients-slider-next',
            prevEl: '.clients-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 6,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();
/* Cover slider */
$(function() {
    let $coverSlider = new Swiper('[data-slider="cover"]', {
        loop: true,
        speed: 750,
        autoplay: {
            delay: 5000,
        },
        grabCursor: true,
        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
});
/* Dropdown menu */
$(document).ready(function(){
    let $dropdownMenuToggle = $('[data-element="dropdown-menu-toggle"]');

    $dropdownMenuToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $dropdown = $toggle.closest('[data-component="dropdown-menu"]');

        $dropdown.toggleClass(visibleClass);
        $body.toggleClass(dropdownMenuVisibleClass);
        $body.toggleClass(lockedScrollClass);
    });
});

Dropzone.autoDiscover = false;

$(document).ready(function () {
    let previewNode = $('[data-element="dropzone-template"]');
    let previewTemplate = previewNode.html();
    previewNode.parent().html('');

    $('[data-dropzone-block]').dropzone({
        maxFiles: 10,
        maxFilesize: 10,
        url: "/ajax_file_upload_handler/",
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: '[data-element="dropzone-previews"]', // Define the container to display the previews
        clickable: '[data-element="dropzone-trigger"]', // Define the element that should be used as click trigger to select files.
        dictFileTooBig: 'Файл  превышает 10 Мб и не будет загружен',
        success: function (file, response) {
            $(file.previewElement).find('[data-dz-uploadprogress]').parent().hide();
        },
        error: function (file, response) {
            let item = $(file.previewElement);
            let itemError = $(file.previewElement).find('[data-dz-errormessage]');
            let itemName = $(file.previewElement).find('[data-dz-name]');

            if (file && response) {
                item.addClass('has-error');
                itemName.hide();
                itemError.html(response)
            } else {
                item.removeClass('has-error');
                itemName.show();
                itemError.html('')
            }
        },
        uploadprogress: function(progress) {
            $(file.previewElement).find('[data-dz-uploadprogress]').css('width', progress + '%');
        },
    });
});
/* Events slider */
$(function() {
    let $eventsSlider = new Swiper('[data-slider="events"]', {
        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,

        navigation: {
            nextEl: '.events-slider-next',
            prevEl: '.events-slider-prev',
        },
    });
});
/* Features slider */
(function() {
    let $featuresSlider = new Swiper('[data-slider="features"]', {
        navigation: {
            nextEl: '.features-slider-next',
            prevEl: '.features-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 7,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();
// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Поле обязательно для заполнения",
    required:       "Поле обязательно для заполнения",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    $(function() {
        let $parsleyForm = $('[data-parsley-validate]');
        let $parsleyFormSubmit = $parsleyForm.find('input[type="submit"]');

        $parsleyForm.parsley({
            excluded: "[disabled], :hidden",
            errorClass: 'has-error',
            successClass: 'has-success',
            errorsWrapper: '<div class="form__errors-list"></div>',
            errorTemplate: '<div class="form__error"></div>',
            errorsContainer (field) {
                return field.$element.parent().closest('.form__group');
            },
            classHandler (field) {
                const $parent = field.$element.closest('.form__group');
                if ($parent.length) return $parent;

                return $parent;
            }
        });

        var checkValid = function(ParsleyForm){
            let submitButton = ParsleyForm.find('input[type="submit"]');

            if ( ParsleyForm.parsley().isValid() ) {
                submitButton.prop('disabled', false);
            } else {
                submitButton.prop('disabled', true);
            }
        }

        $parsleyForm.on('parsley:form:success', function(ParsleyForm) {
            ParsleyForm.$element.find('input[type="submit"]').prop('disabled', false);
        });

        $parsleyForm.on('parsley:form:error', function(ParsleyForm) {
            ParsleyForm.$element.find('input[type="submit"]').prop('disabled', true);
        });

        $('[data-parsley-validate] :input').on('input', function() {
            let ParsleyForm =  $(this).closest('[data-parsley-validate]');
            checkValid(ParsleyForm);
        });

        $('[data-form="request-form"]').on('submit', function(e) {
            e.preventDefault();

            const $form = $(this);
            const $message = $('[data-component="success-message"]');

            if ( $form.parsley().isValid() ) {
                $form.prop('hidden', true);
                $message.prop('hidden', false);
                $form.trigger('reset');
            }
        });
    });

});
/* Gallery slider */
$(function() {
    const $galleryThumbs = new Swiper('[data-slider="gallery-thumbs"]', {
        slidesPerView: 'auto',
        spaceBetween: 13,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        slideToClickedSlide: true,
    });

    const $gallerySlider = new Swiper('[data-slider="gallery"]', {
        loop: true,
        navigation: {
            nextEl: '.gallery-thumbs-next',
            prevEl: '.gallery-thumbs-prev',
        },
        thumbs: {
            swiper: $galleryThumbs,
        },
    });
});
/* Has touch */
function hasTouch() {
    return 'ontouchstart' in document.documentElement
    || navigator.maxTouchPoints > 0
    || navigator.msMaxTouchPoints > 0;
}
/* Header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    (function () {
        const $header = document.querySelector('[data-component="header"]');

        if (!$header) {
            return false;
        }

        if ($header.classList.contains(headerTransparentClass)) {
            $header.setAttribute('data-transparent', true);
        }

        let checkHeaderHeight = function () {
            let headerHeight = $header.offsetHeight;

            if (!document.querySelector('.body').classList.contains('page-home')) {
                document.querySelector('.main').style.paddingTop = headerHeight + 'px';
            }
        }

        let onScroll = debounce(function () {
            $header.classList.toggle(scrolledClass, window.scrollY > 0);
            checkHeaderHeight();
            if ($header.dataset.transparent) {
                $header.classList.toggle(headerTransparentClass, !window.scrollY > 0);
            }
        }, 0);

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    })();
});
/* Hero slider */
$(function() {
    const $heroSlider = new Swiper('[data-slider="hero"]', {
        loop: true,
        speed: 750,
        autoplay: {
            delay: 5000,
        },
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        loopedSlides: 0,
        slidesPerGroup: 1,
          coverflowEffect: {
            rotate: 0,
            stretch: 200,
            depth: 200,
            modifier: 1,
            slideShadows: true,
        },
        navigation: {
            nextEl: '.hero-slider-next',
            prevEl: '.hero-slider-prev',
        },
        pagination: {
            el: '.hero-slider-pagination',
            clickable: true
        },
    });
});
/* Lang switch */
$(document).ready(function(){

    let $langSwitch = $('[data-component="lang-switch"]');
    let $langSwitchToggle = $('[data-element="lang-switch-toggle"]');
    //let $langSwitchItem = $('[data-element="lang-switch-item"]');

    $langSwitchToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $component = $toggle.closest('[data-component="lang-switch"]');

        $component.toggleClass(activeClass);
    });

    /*$langSwitchItem.on('click', function(e) {
        e.preventDefault();

        let $item = $(this);
        let itemCode = $item.attr('data-country-code');
        let itemName = $item.attr('data-country-name');
        let $component = $item.closest('[data-component="lang-switch"]');
        let $toggle = $component.find('[data-element="lang-switch-toggle"]');

        let selectedToggleHtml = `
            <span class="country is-active">
                <img src="/assets/img/flags/${itemCode}.svg" alt="${itemName}" class="country__flag">
                <span class="country__name">${itemName}</span>
            </span>
        `;

        $toggle.html(selectedToggleHtml);

        $item.siblings().removeClass(activeClass);
        $item.siblings().find('.country').removeClass(activeClass);
        $item.addClass(activeClass);
        $item.find('.country').addClass(activeClass);

        $component.toggleClass(activeClass);
    });*/

});

/* Masked input */
$(document).ready(function(){
    $(":input[data-inputmask]").inputmask({
        showMaskOnHover: false,
    });

    /* Masked input fix For parsley validation */
    $(document).on('keypress', function(evt) {
        if(evt.isDefaultPrevented()) {
            // Assume that's because of maskedInput
            // See https://github.com/guillaumepotier/Parsley.js/issues/1076
            $(evt.target).trigger('input');
        }
    });
});
/* Menu */
$(document).ready(function(){

    let $menu = $('[data-component="menu"]');
    let $menuItem = $('[data-component="menu"][data-hover="true"] [data-element="menu-item"]');
    let $menuItemToggle = $('[data-element="menu-item-toggle"]');

    $menuItemToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $item = $toggle.closest('[data-element="menu-item"]');

        $item.toggleClass(collapsedClass);
    });

    $menuItem.on('mouseenter', function(e) {
        $( this ).removeClass(collapsedClass);
    });
});

/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuToggle = $('[data-element="mobile-menu-toggle"]');

function hideMobileMenu() {
    $mobileMenuToggle.removeClass(activeClass);
    $mobileMenu.removeClass(visibleClass);
    $mobileMenu.find('[data-element="menu-item"]').removeClass(activeClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(lockedScrollClass);
    }

    $body.removeClass(mobileMenuVisibleClass);
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuToggle.on('click', function(e) {
        e.preventDefault();

        if ($mobileMenuToggle.hasClass(activeClass)) {
            hideMobileMenu();
        } else {
            $mobileMenuToggle.addClass(activeClass);
            $mobileMenu.addClass(visibleClass);
            $body.addClass(lockedScrollClass);
            $body.addClass(mobileMenuVisibleClass);
        }
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideMobileMenu();
        }
    });

    $(document).on('click', function(e) {
        if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuToggle.has(e.target).length === 0 && !$mobileMenuToggle.is(e.target)) {
            hideMobileMenu();
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    }); 
});
/* Modal */
$(document).ready(function(){
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'mfp-zoom-in',
    });

    /* Modal with video */
    $('[data-element="video-trigger"]').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-zoom-in',
        removalDelay: 160,
        preloader: false,
        closeBtnInside: false,
        gallery: {
          enabled: true
        },
        fixedContentPos: false,
    });

    /* Gallery Popup */
    $('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: true,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
          titleSrc: function(item) {
            return item.el.attr('title');
          }
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    /* Show single image */
    $('[data-element="image-modal-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
    });

    /* Show single image form a text link trigger */
    $('[data-element="text-image-modal-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
        },
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    /* Lang modal visible on page load */
    /*$(window).on('load', function () {
        $.magnificPopup.open({
            items: {
                src: '#modal-lang'
            },
            type: 'inline',
            fixedContentPos: true,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'mfp-zoom-in',
        }, 0);
    });*/
});
/* Panel */
$(document).ready(function(){
    let $panelToggle = $('[data-element="panel-toggle"]');
    let $panelTrigger = $('[data-element="panel-trigger"]');

    $panelToggle.on('click', function(e){
        e.preventDefault();

        let $toggle = $(this);
        let $panel = $toggle.closest('[data-component="panel"]');
        let $trigger = $panel.find('[data-element="panel-trigger"]');

        $panel.toggleClass(collapsedClass);
        $trigger.toggle();
    });

    $panelTrigger.on('click', function(e){
        e.preventDefault();

        let $trigger = $(this);
        let $panel = $trigger.closest('[data-component="panel"]');

        $trigger.hide();
        $panel.removeClass(collapsedClass);
    });
});
/* Perfect scrollbar */
(function() {
    const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

    if (!psContainers) {
        return false;
    }

    for (const container of psContainers) {
        const ps = new PerfectScrollbar(container);
    }
})();

/* Product slider */
$(function() {
    const $productThumbs = new Swiper('[data-slider="product-thumbs"]', {
        slidesPerView: 'auto',
        spaceBetween: 13,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        slideToClickedSlide: true,
    });

    const $productSlider = new Swiper('[data-slider="product"]', {
        loop: true,
        navigation: {
            nextEl: '.product-thumbs-next',
            prevEl: '.product-thumbs-prev',
        },
        thumbs: {
            swiper: $productThumbs,
        },
    });
});
/* Projects slider */
$(function() {
    let $projectsSlider = new Swiper('[data-slider="projects"]', {
        navigation: {
            nextEl: '.projects-slider-next',
            prevEl: '.projects-slider-prev',
        },

        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
});
/* Request popup */
let $requestPopup = $('[data-component="request-popup"]');
let $requestPopupTrigger = $('[data-element="request-popup-trigger"]');
let $requestPopupClose = $requestPopup.find('[data-element="request-popup-close"]');
let $requestPopupForm = $requestPopup.find('[data-form="request-form"]');
let $requestPopupMessage = $requestPopup.find('[data-component="success-message"]');

function hideRequestPopup() {
    $requestPopup.removeClass(visibleClass);

    if ($body.hasClass(requestPopupVisibleClass)) {
       $body.removeClass(lockedScrollClass);
    }

    $body.removeClass(requestPopupVisibleClass);

    $requestPopupForm.prop('hidden', false);
    $requestPopupMessage.prop('hidden', true);
}

function openRequestPopup() {
    if (!$body.hasClass(requestPopupVisibleClass)) {
        $body.addClass(requestPopupVisibleClass);
        $body.addClass(lockedScrollClass);

        $requestPopup.addClass(visibleClass);
    }
}

$(document).ready(function(){

    $requestPopupTrigger.on('click', function(e) {
        e.preventDefault();

        openRequestPopup();
    });

    $requestPopupClose.on('click', function(e) {
        e.preventDefault();

        hideRequestPopup();
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideRequestPopup();
        }
    });
});

/* Search */
let $search = $('[data-component="search"]');
let $searchInput = $search.find('input[type="text"]');
let $searchPopup = $('[data-component="search-popup"]');
let $searchPopupToogle = $('[data-element="search-popup-toggle"]');
let $searchPopupClose = $search.find('[data-element="search-popup-close"]');
let headerClassFlag = false;

function hideSearchPopup() {
    $searchPopup.removeClass(visibleClass);
    //$searchPopup.css('top', '');
    $searchInput.blur();

    if (headerClassFlag === true) {
        $header.addClass(headerTransparentClass);
        headerClassFlag = false;
    }

    if ($body.hasClass(searchVisibleClass)) {
       $body.removeClass(lockedScrollClass);
    }

    $body.removeClass(searchVisibleClass);
}

function openSearchPopup() {
    let headerHeight = $header.height();

    if (!$body.hasClass(searchVisibleClass)) {
        $body.addClass(lockedScrollClass);
        $body.addClass(searchVisibleClass);

        headerHeight = $header.height();

        $searchPopup.css('top', headerHeight - 1 + 'px');
        $searchPopup.addClass(visibleClass);
    }

    if ($header.hasClass(headerTransparentClass)) {
        headerClassFlag = true;
        $header.removeClass(headerTransparentClass);
    }
}

function checkHeaderHeight() {
    let headerHeight = $header.height();

    $searchPopup.css('top', headerHeight - 1 + 'px');

    if (!$body.hasClass('page-home')) {
        $main.css('padding-top', headerHeight + 'px');
    }
}

$(document).ready(function(){

    $searchInput.on('focus', function(e) {
        e.preventDefault();

        openSearchPopup();
    });

    $searchPopupToogle.on('click', function(e) {
        e.preventDefault();

        openSearchPopup();
    });

    $searchPopupClose.on('click', function(e) {
        e.preventDefault();

        hideSearchPopup();
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideSearchPopup();
        }
    });

    checkHeaderHeight();

    $(window).on('orientationchange resize', function() {
        checkHeaderHeight();
    }); 
});

/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        minimumResultsForSearch: Infinity,
        width: '100%'
    };

    $('[data-element="select"]').select2(selectCommonOptions);
});
/* Services slider */
(function() {
    let $servicesSlider = new Swiper('[data-slider="services"]', {
        navigation: {
            nextEl: '.services-slider-next',
            prevEl: '.services-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 4,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();
/* Showcase slider */
(function() {
    let $showcaseSlider = new Swiper('[data-slider="showcase"]', {
        navigation: {
            nextEl: '.showcase-slider-next',
            prevEl: '.showcase-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 3,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();
/*! smooth-scroll v12.1.5 | (c) 2017 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t="querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype,n={ignore:"[data-scroll-ignore]",header:null,speed:500,offset:0,easing:"easeInOutCubic",customEasing:null,before:function(){},after:function(){}},o=function(){for(var e={},t=0,n=arguments.length;t<n;t++){var o=arguments[t];!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(o)}return e},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},r=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,a=-1,r="",i=n.charCodeAt(0);++a<o;){if(0===(t=n.charCodeAt(a)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===a&&t>=48&&t<=57||1===a&&t>=48&&t<=57&&45===i?r+="\\"+t.toString(16)+" ":r+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(a):"\\"+n.charAt(a)}return"#"+r},i=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},u=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},c=function(e,t,n){var o=0;if(e.offsetParent)do{o+=e.offsetTop,e=e.offsetParent}while(e);return o=Math.max(o-t-n,0)},s=function(e){return e?a(e)+e.offsetTop:0},l=function(t,n,o){o||(t.focus(),document.activeElement.id!==t.id&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},f=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)};return function(a,d){var m,h,g,p,v,b,y,S={};S.cancelScroll=function(){cancelAnimationFrame(y)},S.animateScroll=function(t,a,r){var f=o(m||n,r||{}),d="[object Number]"===Object.prototype.toString.call(t),h=d||!t.tagName?null:t;if(d||h){var g=e.pageYOffset;f.header&&!p&&(p=document.querySelector(f.header)),v||(v=s(p));var b,y,E,I=d?t:c(h,v,parseInt("function"==typeof f.offset?f.offset():f.offset,10)),O=I-g,A=u(),C=0,w=function(n,o){var r=e.pageYOffset;if(n==o||r==o||(g<o&&e.innerHeight+r)>=A)return S.cancelScroll(),l(t,o,d),f.after(t,a),b=null,!0},Q=function(t){b||(b=t),C+=t-b,y=C/parseInt(f.speed,10),y=y>1?1:y,E=g+O*i(f,y),e.scrollTo(0,Math.floor(E)),w(E,I)||(e.requestAnimationFrame(Q),b=t)};0===e.pageYOffset&&e.scrollTo(0,0),f.before(t,a),S.cancelScroll(),e.requestAnimationFrame(Q)}};var E=function(e){h&&(h.id=h.getAttribute("data-scroll-id"),S.animateScroll(h,g),h=null,g=null)},I=function(t){if(!f()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&(g=t.target.closest(a))&&"a"===g.tagName.toLowerCase()&&!t.target.closest(m.ignore)&&g.hostname===e.location.hostname&&g.pathname===e.location.pathname&&/#/.test(g.href)){var n;try{n=r(decodeURIComponent(g.hash))}catch(e){n=r(g.hash)}if("#"===n){t.preventDefault(),h=document.body;var o=h.id?h.id:"smooth-scroll-top";return h.setAttribute("data-scroll-id",o),h.id="",void(e.location.hash.substring(1)===o?E():e.location.hash=o)}h=document.querySelector(n),h&&(h.setAttribute("data-scroll-id",h.id),h.id="",g.hash===e.location.hash&&(t.preventDefault(),E()))}},O=function(e){b||(b=setTimeout((function(){b=null,v=s(p)}),66))};return S.destroy=function(){m&&(document.removeEventListener("click",I,!1),e.removeEventListener("resize",O,!1),S.cancelScroll(),m=null,h=null,g=null,p=null,v=null,b=null,y=null)},S.init=function(a){t&&(S.destroy(),m=o(n,a||{}),p=m.header?document.querySelector(m.header):null,v=s(p),document.addEventListener("click",I,!1),e.addEventListener("hashchange",E,!1),p&&e.addEventListener("resize",O,!1))},S.init(d),S}}));

var scroll = new SmoothScroll('[data-scroll]', {
	speed: 1000,
	easing: 'easeInOutCubic',
    header: '[data-component="header"]'
});
/* Menu */
$(document).ready(function(){

    $('td').hover(function() {
        var t = parseInt($(this).index()) + 1;
        $(this).parents('table').find('td:nth-child(' + t + ')').addClass('is-hovered');
    },
    function() {
        var t = parseInt($(this).index()) + 1;
        $(this).parents('table').find('td:nth-child(' + t + ')').removeClass('is-hovered');
    });
});
