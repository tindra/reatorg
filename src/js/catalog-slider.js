/* Catalog slider */
(function() {
    let $catalogSlider = new Swiper('[data-slider="catalog"]', {
        navigation: {
            nextEl: '.catalog-slider-next',
            prevEl: '.catalog-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1220: {
                slidesPerView: 3,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();