/* Clients slider */
(function() {
    let $clientsSlider = new Swiper('[data-slider="clients"]', {
        navigation: {
            nextEl: '.clients-slider-next',
            prevEl: '.clients-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 6,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();