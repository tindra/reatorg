/* Cover slider */
$(function() {
    let $coverSlider = new Swiper('[data-slider="cover"]', {
        loop: true,
        speed: 750,
        autoplay: {
            delay: 5000,
        },
        grabCursor: true,
        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
});