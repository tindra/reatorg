/* Dropdown menu */
$(document).ready(function(){
    let $dropdownMenuToggle = $('[data-element="dropdown-menu-toggle"]');

    $dropdownMenuToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $dropdown = $toggle.closest('[data-component="dropdown-menu"]');

        $dropdown.toggleClass(visibleClass);
        $body.toggleClass(dropdownMenuVisibleClass);
        $body.toggleClass(lockedScrollClass);
    });
});
