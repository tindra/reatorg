/* Events slider */
$(function() {
    let $eventsSlider = new Swiper('[data-slider="events"]', {
        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,

        navigation: {
            nextEl: '.events-slider-next',
            prevEl: '.events-slider-prev',
        },
    });
});