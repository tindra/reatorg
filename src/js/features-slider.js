/* Features slider */
(function() {
    let $featuresSlider = new Swiper('[data-slider="features"]', {
        navigation: {
            nextEl: '.features-slider-next',
            prevEl: '.features-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 7,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();