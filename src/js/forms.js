// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Поле обязательно для заполнения",
    required:       "Поле обязательно для заполнения",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    $(function() {
        let $parsleyForm = $('[data-parsley-validate]');
        let $parsleyFormSubmit = $parsleyForm.find('input[type="submit"]');

        $parsleyForm.parsley({
            excluded: "[disabled], :hidden",
            errorClass: 'has-error',
            successClass: 'has-success',
            errorsWrapper: '<div class="form__errors-list"></div>',
            errorTemplate: '<div class="form__error"></div>',
            errorsContainer (field) {
                return field.$element.parent().closest('.form__group');
            },
            classHandler (field) {
                const $parent = field.$element.closest('.form__group');
                if ($parent.length) return $parent;

                return $parent;
            }
        });

        var checkValid = function(ParsleyForm){
            let submitButton = ParsleyForm.find('input[type="submit"]');

            if ( ParsleyForm.parsley().isValid() ) {
                submitButton.prop('disabled', false);
            } else {
                submitButton.prop('disabled', true);
            }
        }

        $parsleyForm.on('parsley:form:success', function(ParsleyForm) {
            ParsleyForm.$element.find('input[type="submit"]').prop('disabled', false);
        });

        $parsleyForm.on('parsley:form:error', function(ParsleyForm) {
            ParsleyForm.$element.find('input[type="submit"]').prop('disabled', true);
        });

        $('[data-parsley-validate] :input').on('input', function() {
            let ParsleyForm =  $(this).closest('[data-parsley-validate]');
            checkValid(ParsleyForm);
        });

        $('[data-form="request-form"]').on('submit', function(e) {
            e.preventDefault();

            const $form = $(this);
            const $message = $('[data-component="success-message"]');

            if ( $form.parsley().isValid() ) {
                $form.prop('hidden', true);
                $message.prop('hidden', false);
                $form.trigger('reset');
            }
        });
    });

});