/* Gallery slider */
$(function() {
    const $galleryThumbs = new Swiper('[data-slider="gallery-thumbs"]', {
        slidesPerView: 'auto',
        spaceBetween: 13,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        slideToClickedSlide: true,
    });

    const $gallerySlider = new Swiper('[data-slider="gallery"]', {
        loop: true,
        navigation: {
            nextEl: '.gallery-thumbs-next',
            prevEl: '.gallery-thumbs-prev',
        },
        thumbs: {
            swiper: $galleryThumbs,
        },
    });
});