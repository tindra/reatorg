/* Header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    (function () {
        const $header = document.querySelector('[data-component="header"]');

        if (!$header) {
            return false;
        }

        if ($header.classList.contains(headerTransparentClass)) {
            $header.setAttribute('data-transparent', true);
        }

        let checkHeaderHeight = function () {
            let headerHeight = $header.offsetHeight;

            if (!document.querySelector('.body').classList.contains('page-home')) {
                document.querySelector('.main').style.paddingTop = headerHeight + 'px';
            }
        }

        let onScroll = debounce(function () {
            $header.classList.toggle(scrolledClass, window.scrollY > 0);
            checkHeaderHeight();
            if ($header.dataset.transparent) {
                $header.classList.toggle(headerTransparentClass, !window.scrollY > 0);
            }
        }, 0);

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    })();
});