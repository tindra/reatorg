/* Hero slider */
$(function() {
    const $heroSlider = new Swiper('[data-slider="hero"]', {
        loop: true,
        speed: 750,
        autoplay: {
            delay: 5000,
        },
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        loopedSlides: 0,
        slidesPerGroup: 1,
          coverflowEffect: {
            rotate: 0,
            stretch: 200,
            depth: 200,
            modifier: 1,
            slideShadows: true,
        },
        navigation: {
            nextEl: '.hero-slider-next',
            prevEl: '.hero-slider-prev',
        },
        pagination: {
            el: '.hero-slider-pagination',
            clickable: true
        },
    });
});