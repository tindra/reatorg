/* Menu */
$(document).ready(function(){

    let $menu = $('[data-component="menu"]');
    let $menuItem = $('[data-component="menu"][data-hover="true"] [data-element="menu-item"]');
    let $menuItemToggle = $('[data-element="menu-item-toggle"]');

    $menuItemToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let $item = $toggle.closest('[data-element="menu-item"]');

        $item.toggleClass(collapsedClass);
    });

    $menuItem.on('mouseenter', function(e) {
        $( this ).removeClass(collapsedClass);
    });
});
