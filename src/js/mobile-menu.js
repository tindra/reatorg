/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuToggle = $('[data-element="mobile-menu-toggle"]');

function hideMobileMenu() {
    $mobileMenuToggle.removeClass(activeClass);
    $mobileMenu.removeClass(visibleClass);
    $mobileMenu.find('[data-element="menu-item"]').removeClass(activeClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(lockedScrollClass);
    }

    $body.removeClass(mobileMenuVisibleClass);
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuToggle.on('click', function(e) {
        e.preventDefault();

        if ($mobileMenuToggle.hasClass(activeClass)) {
            hideMobileMenu();
        } else {
            $mobileMenuToggle.addClass(activeClass);
            $mobileMenu.addClass(visibleClass);
            $body.addClass(lockedScrollClass);
            $body.addClass(mobileMenuVisibleClass);
        }
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideMobileMenu();
        }
    });

    $(document).on('click', function(e) {
        if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuToggle.has(e.target).length === 0 && !$mobileMenuToggle.is(e.target)) {
            hideMobileMenu();
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    }); 
});