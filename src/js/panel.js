/* Panel */
$(document).ready(function(){
    let $panelToggle = $('[data-element="panel-toggle"]');
    let $panelTrigger = $('[data-element="panel-trigger"]');

    $panelToggle.on('click', function(e){
        e.preventDefault();

        let $toggle = $(this);
        let $panel = $toggle.closest('[data-component="panel"]');
        let $trigger = $panel.find('[data-element="panel-trigger"]');

        $panel.toggleClass(collapsedClass);
        $trigger.toggle();
    });

    $panelTrigger.on('click', function(e){
        e.preventDefault();

        let $trigger = $(this);
        let $panel = $trigger.closest('[data-component="panel"]');

        $trigger.hide();
        $panel.removeClass(collapsedClass);
    });
});