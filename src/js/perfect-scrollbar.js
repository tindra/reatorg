/* Perfect scrollbar */
(function() {
    const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

    if (!psContainers) {
        return false;
    }

    for (const container of psContainers) {
        const ps = new PerfectScrollbar(container);
    }
})();
