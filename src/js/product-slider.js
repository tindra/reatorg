/* Product slider */
$(function() {
    const $productThumbs = new Swiper('[data-slider="product-thumbs"]', {
        slidesPerView: 'auto',
        spaceBetween: 13,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        slideToClickedSlide: true,
    });

    const $productSlider = new Swiper('[data-slider="product"]', {
        loop: true,
        navigation: {
            nextEl: '.product-thumbs-next',
            prevEl: '.product-thumbs-prev',
        },
        thumbs: {
            swiper: $productThumbs,
        },
    });
});