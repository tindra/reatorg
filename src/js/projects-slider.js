/* Projects slider */
$(function() {
    let $projectsSlider = new Swiper('[data-slider="projects"]', {
        navigation: {
            nextEl: '.projects-slider-next',
            prevEl: '.projects-slider-prev',
        },

        slidesPerView: 1,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
});