/* Request popup */
let $requestPopup = $('[data-component="request-popup"]');
let $requestPopupTrigger = $('[data-element="request-popup-trigger"]');
let $requestPopupClose = $requestPopup.find('[data-element="request-popup-close"]');
let $requestPopupForm = $requestPopup.find('[data-form="request-form"]');
let $requestPopupMessage = $requestPopup.find('[data-component="success-message"]');

function hideRequestPopup() {
    $requestPopup.removeClass(visibleClass);

    if ($body.hasClass(requestPopupVisibleClass)) {
       $body.removeClass(lockedScrollClass);
    }

    $body.removeClass(requestPopupVisibleClass);

    $requestPopupForm.prop('hidden', false);
    $requestPopupMessage.prop('hidden', true);
}

function openRequestPopup() {
    if (!$body.hasClass(requestPopupVisibleClass)) {
        $body.addClass(requestPopupVisibleClass);
        $body.addClass(lockedScrollClass);

        $requestPopup.addClass(visibleClass);
    }
}

$(document).ready(function(){

    $requestPopupTrigger.on('click', function(e) {
        e.preventDefault();

        openRequestPopup();
    });

    $requestPopupClose.on('click', function(e) {
        e.preventDefault();

        hideRequestPopup();
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideRequestPopup();
        }
    });
});
