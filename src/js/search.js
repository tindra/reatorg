/* Search */
let $search = $('[data-component="search"]');
let $searchInput = $search.find('input[type="text"]');
let $searchPopup = $('[data-component="search-popup"]');
let $searchPopupToogle = $('[data-element="search-popup-toggle"]');
let $searchPopupClose = $search.find('[data-element="search-popup-close"]');
let headerClassFlag = false;

function hideSearchPopup() {
    $searchPopup.removeClass(visibleClass);
    //$searchPopup.css('top', '');
    $searchInput.blur();

    if (headerClassFlag === true) {
        $header.addClass(headerTransparentClass);
        headerClassFlag = false;
    }

    if ($body.hasClass(searchVisibleClass)) {
       $body.removeClass(lockedScrollClass);
    }

    $body.removeClass(searchVisibleClass);
}

function openSearchPopup() {
    let headerHeight = $header.height();

    if (!$body.hasClass(searchVisibleClass)) {
        $body.addClass(lockedScrollClass);
        $body.addClass(searchVisibleClass);

        headerHeight = $header.height();

        $searchPopup.css('top', headerHeight - 1 + 'px');
        $searchPopup.addClass(visibleClass);
    }

    if ($header.hasClass(headerTransparentClass)) {
        headerClassFlag = true;
        $header.removeClass(headerTransparentClass);
    }
}

function checkHeaderHeight() {
    let headerHeight = $header.height();

    $searchPopup.css('top', headerHeight - 1 + 'px');

    if (!$body.hasClass('page-home')) {
        $main.css('padding-top', headerHeight + 'px');
    }
}

$(document).ready(function(){

    $searchInput.on('focus', function(e) {
        e.preventDefault();

        openSearchPopup();
    });

    $searchPopupToogle.on('click', function(e) {
        e.preventDefault();

        openSearchPopup();
    });

    $searchPopupClose.on('click', function(e) {
        e.preventDefault();

        hideSearchPopup();
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideSearchPopup();
        }
    });

    checkHeaderHeight();

    $(window).on('orientationchange resize', function() {
        checkHeaderHeight();
    }); 
});
