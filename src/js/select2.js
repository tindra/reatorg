/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        minimumResultsForSearch: Infinity,
        width: '100%'
    };

    $('[data-element="select"]').select2(selectCommonOptions);
});