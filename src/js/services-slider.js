/* Services slider */
(function() {
    let $servicesSlider = new Swiper('[data-slider="services"]', {
        navigation: {
            nextEl: '.services-slider-next',
            prevEl: '.services-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 4,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();