/* Showcase slider */
(function() {
    let $showcaseSlider = new Swiper('[data-slider="showcase"]', {
        navigation: {
            nextEl: '.showcase-slider-next',
            prevEl: '.showcase-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 'auto',
            },
            1440: {
                slidesPerView: 3,
            },
        },

        loop: false,
        observer: true,
        observeParents: true,
        speed: 800,
        spaceBetween: 0,
        watchOverflow: true,
    });
})();