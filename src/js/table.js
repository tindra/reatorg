/* Menu */
$(document).ready(function(){

    $('td').hover(function() {
        var t = parseInt($(this).index()) + 1;
        $(this).parents('table').find('td:nth-child(' + t + ')').addClass('is-hovered');
    },
    function() {
        var t = parseInt($(this).index()) + 1;
        $(this).parents('table').find('td:nth-child(' + t + ')').removeClass('is-hovered');
    });
});
